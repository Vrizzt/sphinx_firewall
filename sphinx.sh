#! /bin/sh
### BEGIN INIT INFO
# Provides:          iptables
# Required-Start:
# Required-Stop:
# Should-Start:
# Should-Stop:
# Default-Start:     1 2 3 4 5
# Default-Stop:      0 6
# Short-Description: script iptables
### END INIT INFO

# activer au demarage.
# en root avec su ou sudo
# cp ...iptables /etc/init.d/
# chown root:root /etc/init.d/iptables
# chmod 750 /etc/init.d/iptables
# update-rc.d iptables defaults
# /etc/init.d/iptables start

interface_WAN=$(ip route | awk '/^default via/{print $5}')  # suppose que la passerelle est la route par default

on="1"
off="0"
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
ipbox=$(ip route | grep default | cut -d " " -f3)   # suppose que la passerelle est la route par default
ipinterface_WAN=$(ifconfig $interface_WAN | grep adr: | cut -d":" -f2 | cut -d" " -f1)
reseau_box=$(ip route | grep / | grep "$interface_WAN" | cut -d" " -f1 )
ip_broadcast=$(ifconfig $interface_WAN | grep Bcast | cut -d":" -f3 | cut -d" " -f1)

##### Services à autoriser ou non ######

ftp=$off
mailssl=$off
mail=$off
clientNTP=$on
cupsServeur=$off
pingexterne=$off
transmition=$off
pidgin=$off

# Services serveur #

webserver=$off

start() {
  ## parametrage pour ce protéger contre les attaques par spoofing et par synflood
  sysctl -w net.ipv4.conf.default.rp_filter=1
  sysctl -w net.ipv4.conf.all.rp_filter=1
  sysctl -w net.ipv4.tcp_syncookies=1
  sysctl -w net.ipv4.tcp_max_syn_backlog=1280

# Vider les tables actuelles
	iptables -t filter -F
	iptables -t nat -F
	iptables -t mangle -F

# Vider les regles personnelles
	iptables -t filter -X
	iptables -t nat -X
	iptables -t mangle -X

# Interdire toute connexion entrante et sortante
	iptables -P INPUT DROP
	iptables -P FORWARD DROP
	iptables -P OUTPUT DROP

### Refuse les scans XMAS et NULL
  iptables -A INPUT -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
  iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP

### Dropper silencieusement tous les paquets broadcastés.

  iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP

### Iptables limite TCP, UDP, ICMP Flood !

  # TCP Syn Flood
  iptables -A INPUT -i $interface_WAN -p tcp --syn -m limit --limit 3/s -j ACCEPT
  # UDP Syn Flood
  iptables -A INPUT -i $interface_WAN -p udp -m limit --limit 10/s -j ACCEPT

### DHCP
  iptables -A OUTPUT -o $interface_WAN -p udp --sport 68 --dport 67 -j ACCEPT
  iptables -A INPUT -i $interface_WAN -p udp --sport 67 --dport 68 -j ACCEPT

### DNS ###
	iptables -A INPUT -m state --state ESTABLISHED,RELATED --protocol udp --source-port 53 -j ACCEPT
	iptables -A INPUT -m state --state ESTABLISHED,RELATED --protocol tcp --source-port 53 -j ACCEPT

	iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED --protocol udp --destination-port 53 -j ACCEPT
	iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED --protocol tcp --destination-port 53 -j ACCEPT

# HTTP + HTTPS avec suivi de connexion
	iptables -A OUTPUT -p tcp --dport 80 -m state --state RELATED,ESTABLISHED,NEW -j ACCEPT
	iptables -A OUTPUT -p tcp --dport 443 -m state --state RELATED,ESTABLISHED,NEW -j ACCEPT

	iptables -A INPUT -p tcp --sport 80 -m state --state ESTABLISHED -j ACCEPT
	iptables -A INPUT -p tcp --sport 443 -m state --state ESTABLISHED -j ACCEPT

### SSH

	iptables -A INPUT -m state --state NEW,ESTABLISHED,RELATED -p tcp --dport 666 -j ACCEPT
	iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -p tcp --sport 666 -j ACCEPT

### ACCEPT ALL interface loopback ###
	iptables -A INPUT -i lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT
	iptables -A OUTPUT -o lo -s 127.0.0.0/8 -d 127.0.0.0/8 -j ACCEPT

### accepte en entrée les connexions déjà établies (en gros cela permet d'accepter
### les connexions initiées par sont propre serveur)
  iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# TODO
### Mise en place d'un blacklist d'ip suivant le tuto sur http://n0where.net/iptables-blacklist-script/
### Utilise le fichier /usr/local/bin/update-blacklist.sh
### Mise à jour automatique par un cron dans /etc/cron.d/update-blacklist dont le contenu est :
### MAILTO=root
### 33 23 * * *      root /usr/local/bin/update-blacklist.sh
# ipset create blacklist hash:net
# iptables -I INPUT -m set --match-set blacklist src -j DROP

# Ping Flood protection
   if [ $pingexterne  -eq $on ];then
      iptables -A INPUT -i $interface_WAN -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
      iptables -A INPUT -i $interface_WAN -p icmp --icmp-type echo-reply -m limit --limit 1/s -j ACCEPT
   fi

    ### FTP avec suivi de connexion
    if [ $ftp=$on ]
    then
	    modprobe ip_conntrack_ftp	# chargement du module permettent le suivi des connections ftp
    	# Etape 1 : Etablissement de la connexion
    	iptables -A OUTPUT -p tcp --dport 21 -m state --state ESTABLISHED,NEW -j ACCEPT
    	iptables -A OUTPUT -p tcp --dport 990 -m state --state ESTABLISHED,NEW -j ACCEPT

    	# Etape 2 : Etablissement du mode actif
    	iptables -A INPUT -p tcp --sport 20 -m state --state ESTABLISHED,RELATED -j ACCEPT
    	iptables -A OUTPUT -p tcp --dport 20 -m state --state ESTABLISHED -j ACCEPT

    	# Etape 3 : Etablissement du mode passif
    	iptables -A INPUT -p tcp --sport 1024:65535 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
    	iptables -A OUTPUT -p tcp --sport 1024:65535 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT
    fi

    ### smtp + pop ssl ####
    if [  $mailssl -eq $on ]
    then
      iptables -A OUTPUT -p tcp --dport 465 -m state --state ESTABLISHED,NEW -j ACCEPT
      iptables -A INPUT -p tcp --sport 465 -m state --state ESTABLISHED -j ACCEPT
      iptables -A OUTPUT -p tcp --dport 995 -m state --state ESTABLISHED,NEW -j ACCEPT
      iptables -A INPUT -p tcp --sport 995 -m state --state ESTABLISHED -j ACCEPT
    fi

    ### clientNTP ... syncro à un serveur de temps ###
    if [  $clientNTP -eq $on ]
    then
       iptables -A OUTPUT -p udp -m udp --dport 123 -j ACCEPT
    fi

    # server web rules
    if [ $webserver  -eq $on ];then
      # HTTP + HTTPS Out
      iptables -t filter -A OUTPUT -p tcp --dport 80 --state ESTABLISHED,RELATED -j ACCEPT
      iptables -t filter -A OUTPUT -p tcp --dport 443 --state ESTABLISHED,RELATED -j ACCEPT

      # HTTP + HTTPS In
      iptables -t filter -A INPUT -p tcp --dport 80 --state NEW,ESTABLISHED,RELATED -j ACCEPT
      iptables -t filter -A INPUT -p tcp --dport 443 --state NEW,ESTABLISHED,RELATED -j ACCEPT
    fi

    ### ping ... autorise à "pinger" un ordinateur distant ###
    iptables -A OUTPUT -p icmp -j ACCEPT

    ### LOG ### Log tout ce qui qui n'est pas accepté par une règles précédente
    iptables -A OUTPUT -j LOG  --log-prefix "iptables: "
    iptables -A INPUT -j LOG   --log-prefix "iptables: "
    iptables -A FORWARD -j LOG  --log-prefix "iptables: "
    echo "############ <START SPHINX FIREWALL> ##############"
    iptables -L -n
}
stop() {
### OUVRE TOUS !! ###
    iptables -F
    iptables -X
    iptables -P INPUT ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -P FORWARD ACCEPT
    echo "############ <STOP SPHINX FIREWALL> ##############"
    iptables -L -n
}

case "$1" in
  start)
   start
    ;;
  stop)
      stop
    ;;
  restart)
   stop
   start
    ;;
  *)
    N=/etc/init.d/${0##*/}
    echo "Usage: $N {start|stop|restart}" >&2
    exit 1
    ;;
esac

exit 0
